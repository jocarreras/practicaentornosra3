package ra3.refactorizacion;

import java.util.Scanner;

/**
*@author JoelCarreras
*since 13-03-2018
*/
public class Refactorizar1 {

	final static String saludo = "Bienvenido al programa";

	public static void main(String[] args) {	

	//Declaramos las cadenas en cada linea

		System.out.println(saludo);
		Scanner c = new Scanner(System.in);
		System.out.println("Introduce tu dni");
		String cadena1 = c.nextLine();
		System.out.println("Introduce tu nombre");
		String cadena2 = c.nextLine();

	//Declaramos cada variable en una linea

		int numeroA = 7;
		int numeroB = 16;
		int numeroC = 25;
		
	
	//Creamos un metodo para comprobar la condicion si se cumple

		calcularC(numeroC, numeroA, numeroB);

		String[] diasSemana= { "Lunes", "Martes", "Miercoles", "Jueves",  "Viernes",
				"Sabado", "Domingo"};
		

	//Creamos un metodo para mostrar que dia de la semana es cada numero
	mostrarDiasSemana(diasSemana);

	}


	private static void calcularC(int numeroA, int numeroB, int numeroC) {
	//Cambiamos las variables por numeroA, numeroB y numeroC
		if (numeroA > numeroB || (numeroC % 5) != 0 && ((numeroC * 3) - 1) > (numeroB / numeroC)) {
			System.out.println("Se cumple la condición");
		}
		numeroC = numeroA + numeroB * numeroC + numeroB / numeroA;
		
	}

	static void mostrarDiasSemana(String vectordestrings[]) {
		for (int dia = 0; dia < 7; dia++) {
			System.out.println("El dia de la semana en el que te encuentras [" + (dia + 1) + "-7] es el dia: "
					+ vectordestrings[dia]);
		}
	}
}
