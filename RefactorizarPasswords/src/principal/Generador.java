package principal;

import java.util.Scanner;

/**
 * 
 * @author Joel
 *@since 30-04-2018
 */
public class Generador {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Menu();
		//Refactorizado lo maximo posible convirtiendo las distintas funciones en metodos
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		String password=" ";
		menuPassword(longitud,password,opcion);
		scanner.close();
	}
//menu de la contraseņa
	private static void menuPassword(int longitud, String password, int opcion) {
		switch (opcion) {
		case 1:
			password=caracteresAZ(longitud,password);
			break;
		case 2:
			password=numeros09(longitud,password);
			break;
		case 3:
			password=letrasCaracteresEspeciales(longitud,password);
			break;
		case 4:
			password=letrasNumerosCaracteresEspeciales(longitud,password);
			break;
		}

		System.out.println(password);
		
	}
//Contraseņa con caracteres especiales
	private static String letrasNumerosCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 3);
			if (n == 1) {
				char letra4;
				letra4 = (char) ((Math.random() * 26) + 65);
				password += letra4;
			} else if (n == 2) {
				char caracter4;
				caracter4 = (char) ((Math.random() * 15) + 33);
				password += caracter4;
			} else {
				int numero4;
				numero4 = (int) (Math.random() * 10);
				password += numero4;
			}
		}
		return password;
	}
//contraseņa con letras especiales
	private static String letrasCaracteresEspeciales(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n = (int) (Math.random() * 2);
			if (n == 1) {
				char letra3;
				letra3 = (char) ((Math.random() * 26) + 65);
				password += letra3;
			} else {
				char caracter3;
				caracter3 = (char) ((Math.random() * 15) + 33);
				password += caracter3;
			}
		}
		return password;
	}


	private static String numeros09(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			password +=numeroAleatorio();
		}
		return password;
	}

	private static int numeroAleatorio() {
		return (int) (Math.random() * 10);
		
		
	}

	private static String caracteresAZ(int longitud, String password) {
		
		for (int i = 0; i < longitud; i++) {
			password+=letraAleatoria();
		}
		return password;
		
	}

	private static char letraAleatoria() {
		
		return (char) ((Math.random() * 26) + 65);
		
	}

	private static void Menu() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}

}
