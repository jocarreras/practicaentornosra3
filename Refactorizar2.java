
package ra3.refactorizacion;

//Borramos el java.io y dejamos solo el scanner importado
import java.util.Scanner;
/**
 * 
 * @author JoelCarreras
 * @since 30-04-2018
 */
public class Refactorizar2 {
	//Pondremos el nombre mas claro a las variables y usaremos las llaves donde se deban usar
		static Scanner in= new Scanner(System.in);
		public static void main(String[] args){
		
			
			int []CantidadAlumnos = new int[10];
			//Ponemos las variables en su sitio
			for(int n=0;n<10;n++){
				System.out.println("Introduce nota media de alumno");
				CantidadAlumnos[n] = in.nextInt();
			}	
			
			System.out.println("El resultado es: " + media(CantidadAlumnos));
			
			in.close();
		}
		//Poner llaves en su lugar y retornar la media
		 static double media(int v[]){
			double media=0;
			for(int a=0;a<10;a++) {
				media=(media+v[a])/10;
			}
			return media;
		}
		
	}